package consul

import (
	"fmt"
	"testing"
)

func TestRegisterServer(t *testing.T) {
	servers := map[string]interface{}{"ID": "go_consul", "Name": "go1", "Address": "127.0.0.1", "Port": 8300}

	fmt.Println("servers : ", servers)

	agent := NewAgent("192.168.169.204", 8500)
	res, _ := agent.RegisterService(servers)
	fmt.Println("res : ", res)
}

func TestServices(t *testing.T) {
	agent := NewAgent("192.168.169.204", 8500)
	res, _ := agent.Services()
	fmt.Println(string(res.Body))
}
