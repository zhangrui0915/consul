package consul

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
)

// 封装的对consul最为基础的操作
type Consul struct {
	host string
	port int
}

// 创建consul操作对象
func NewConsul(host string, port int) *Consul {
	return &Consul{
		host: host,
		port: port,
	}
}
func (c *Consul) Get(url string, options map[string]interface{}) (*Response, error) {
	return c.request("GET", url, options)
}
func (c *Consul) Put(url string, options map[string]interface{}) (*Response, error) {
	return c.request("PUT", url, options)
}

// 发送consul请求
func (c *Consul) request(method, url string, options map[string]interface{}) (*Response, error) {
	// 确定请求的api
	uri := "http://" + c.host + ":" + strconv.Itoa(c.port) + url
	var req *http.Request
	// 根据请求是否携带参数进行参数设置处理
	if options != nil {
		s, _ := json.Marshal(options)
		req, _ = http.NewRequest(method, uri, bytes.NewReader(s))
	} else {
		req, _ = http.NewRequest(method, uri, nil)
	}
	// 请求api
	res, _ := http.DefaultClient.Do(req)
	if res == nil {
		return nil, errors.New("consul请求失败；请检测访问地址和端口")
	}
	// 返回请求结构
	return NewResponse(res), nil
}
